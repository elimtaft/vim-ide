# Vim Python IDE
This project provides some basic tools that are helpful for developing Python applications using the Vim editor.

## Install
To install this project::

    cd ~
    sudo apt-get update && sudo apt-get install vim python3-pip python3-pyflakes pyflakes3
    
    # If these exist, move them:
    mv .vim .vimBK
    mv .vimrc .vimrcBK
    
    git clone git@bitbucket.org:elimtaft/vim-ide.git .vim
    ln -s $HOME/.vim/.vimrc $HOME/.vimrc
    cd .vim
    mkdir bundle
    git clone https://github.com/gmarik/Vundle.vim.git bundle/Vundle.vim
    vim +PluginInstall +qall

Note: The last command will probably show you errors the first time you run it.
This is because the .vimrc file includes settings for plugins that aren't installed yet.
The last command above installs those plugins, so the error shouldn't happen again.

## Installing / Removing Plugin Submodules
To install a new plugin, just add it to the .vimrc file, right below the other "Plugin" definitions.
For information on how to format the Plugin definition, see the [vundle](https://github.com/VundleVim/Vundle.vim) README file.
After you add a new plugin definition, execute this from the command-line::

    vim +PluginUpdate +qall

To uninstall a vim plugin Git submodule, remove it from the .vimrc file and execute::

    vim +PluginUpdate +qall

## Contributing
Feel free to clone it, use it, modify it, fork it, love it, hate it, etc.
