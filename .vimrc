
set nocompatible

" --------------------
" vundle BEGIN
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'vim-scripts/Colour-Sampler-Pack'
Plugin 'vim-scripts/ScrollColors'
Plugin 'nvie/vim-flake8'
" Plugin 'w0rp/ale'
Plugin 'SirVer/ultisnips'
Plugin 'sjl/gundo.vim'
Plugin 'vim-scripts/pydoc.vim'
Plugin 'ervandew/supertab'
call vundle#end()
" vundle END
" --------------------

" --------------------------
" Ultisnips BEGIN
" Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
set rtp+=~/.vim/ultisnips
let g:UltiSnipsExpandTrigger='<Tab>'
let g:UltiSnipsJumpForwardTrigger='<Tab>'
let g:UltiSnipsJumpBackwardTrigger='<S-Tab>'
let g:UltiSnipsListSnippets='<C-L>'
let g:UltiSnipsSnippetsDir = '/home/elimtaft/.vim/ultisnips'
let g:UltiSnipsSnippetDirectories = ['/home/elimtaft/.vim/ultisnips']
" Ultisnips END
" --------------------

" syntax highlighting
syntax on

" Enable filetype detection / specific plugins / indentation
filetype plugin indent on

" indentation
set autoindent

" visual
set cursorline
set t_Co=256
set term=xterm-256color

" colorpack
colorscheme fnaqevan

" gundo
nnoremap <F5> :GundoToggle<CR>

" Give a shortcut key to NERD Tree
map <F2> :NERDTreeToggle<CR>
map <F3> :w<CR> :!clear; make<CR> :!./%<<CR>
nnoremap <silent> <Space>N :NERDTreeFind<CR>


set tabstop=4 softtabstop=4 shiftwidth=4 expandtab

" These things are only set for Python files:
" -------------------------------------------

" Change tabs to 4 spaces
" au FileType python set tabstop=4 softtabstop=4 shiftwidth=4 expandtab

" pep8 width
au FileType python let &colorcolumn="80"

" Remove all trailing whitespace on save:
au FileType python autocmd BufWritePre * %s/\s\+$//e

" supertab
au FileType python set omnifunc=pythoncomplete#Complete
au FileType python let g:SuperTabDefaultCompletionType = "context"
au FileType python set completeopt=menuone,longest,preview

" Run Flake8 on save
au FileType python autocmd BufWritePost *.py call Flake8()


" These things are only set for C/C++ files:
" -------------------------------------------

" Change tabs to 4 spaces
" au FileType c set tabstop=4 softtabstop=4 shiftwidth=4 expandtab
" au FileType cpp set tabstop=4 softtabstop=4 shiftwidth=4 expandtab

" pep8 width
au FileType c let &colorcolumn="80"
au FileType cpp let &colorcolumn="80"

" Remove all trailing whitespace on save:
au FileType c autocmd BufWritePre * %s/\s\+$//e
au FileType cpp autocmd BufWritePre * %s/\s\+$//e

" supertab
au FileType c set omnifunc=pythoncomplete#Complete
au FileType c let g:SuperTabDefaultCompletionType = "context"
au FileType c set completeopt=menuone,longest,preview
au FileType cpp set omnifunc=pythoncomplete#Complete
au FileType cpp let g:SuperTabDefaultCompletionType = "context"
au FileType cpp set completeopt=menuone,longest,preview


